// console.log("Hello World");

// [SECTION] if, else, else if, statement
// Conditional Statements
	// It allows us to control the flow of our program. It allows us to run a statement or instruction if a condition is met.

let numA = -1;

// if Statement
// Executes a statement if a specified condition is 'true'
/*

SYNTAX:

if(condtion){
	//codeblock or statement
}


*/

if(numA < 0){
	console.log("Hello");
}

// kapag mali or false yung condition hindi lalabas sa console yung "hello"

// Basic checking
console.log(numA < 0);

// Let's update numA and run our system
numA = 0;

if(numA < 0){
	console.log("Hello Again if numA is 0");
}

// Another example

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York");
}

// else if clause\
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numH = 1;

if(numA < 0){
	console.log("Hello");
} else if (numH > 0){
	console.log("World");
}

numA = 1;

if(numA > 0){
	console.log("Hello");
} else if (numH > 0){
	console.log("World");
} else if (numH < 0){
	console.log("Again");
}

// else statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

if(numA < 0){
	console.log("Hello");
}else if (numH === 0){
	console.log("World");
}else{
	console.log("Again");
}

// else{
// 	console.log("test");
// }

// We cannot use else alone dapat kasama nya si if at else if pati na rin si else if di din 	pwede alone
// else at else di din gagana pag walang if (dapt lagi present si if)
// si if kaya nya magisa
// di pwedenng mauna si else kay else if
// if then else if then else

// Avoid using this structure -> if, else, else if

// else if(numH === 0){
// 	console.log("world");
// }else{
// 	console.log("Again")
// }


// if, else if, else statements with functions

let message = "No Message";
console.log(message);


function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return "Not a typhoon yet.";
	}else if(windspeed <= 61){
		return "Tropical Depression Detected.";
	}else if(windspeed >= 62 && windspeed <= 88){
		return "Tropical Storm Detected."
	}else if(windspeed >= 89 && windspeed <= 117){
		return "Severe Tropical Storm Detected.";
	}else{
		return "Typhoon Detected."
	}
}

message = determineTyphoonIntensity(75);
console.log(message);

if (message == "Tropical Storm Detected."){
	console.error(message);
}

// Truthy and falsy
// truthy

if(true){
	console.log("truthy");
}

// 1 is equivalent to true
// [] is equivalent to true

if(1){
	console.log("truthy");
}

if([]){
	console.log("truthy");
}

// falsy
// 0 is equivalent to false
// undefine is equivalent to false

// pag false walang lalabas sa console

if(false){
	console.log("falsy");
}

if(0){
	console.log("falsy");
}

if(undefined){
	console.log("falsy");
}

// [SECTION] Conditional or Ternary operator
/* 
    - The Conditional (Ternary) Operator takes in three operands:
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator

    - Syntax
        (expression) ? ifTrue : ifFalse;
*/

// Single statement execution

// ? = if 
// : = else
let ternaryResult = (1 > 18) ? true : false;

console.log("Result of ternary operator: " + ternaryResult);

// multiple statement execution

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit.";
}

function isUnderAge(){
	name = "Jane";
	return "You are under the age limit.";
}

let age = parseInt(prompt("What is your age?"))
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of ternary Operator in functions: " + legalAge + ", " + name);

// [SECTION] Switch Statement

/*
SYNTAX

switch(expression){
	case value: 
		statement;
		break;
	default: 
		statement;
		break;
}

*/

let day = prompt("What day of the week is it today").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is red");
		break;
	case "tuesday":
		console.log("The color of the day is orange");
		break;
	case "wedneday":
		console.log("The color of the day is yellow");
		break;
	case "thursday":
		console.log("The color of the day is green");
		break;
	case "friday":
		console.log("The color of the day is blue");
		break;
	case "saturday":
		console.log("The color of the day is indigo");
		break;
	case "sunday":
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
		break;
}

// [SECTIOn] Try-Catch-Finally

function showIntensityAlert(windspeed){
	try{
		alerat(determineTyphoonIntensity(windspeed)); // sinadya yung wrong spelling
	}catch (error){
		console.log(typeof error);
		console.warn(error.message) // -> dafault
	}finally{
		// continue the execution of the code
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);