// console.log("Hello world");

// Arithmetic Operators

let x = 1397, y = 7831;

let sum = x + y;
console.log("Result of addition operator" + sum);

let	difference = x - y;
console.log("Result of substraction operator" + difference);

let	product = x * y;
console.log("Result of multiplication operator" + product);

let	qoutient = x / y;
console.log("Result of division operator" + qoutient);

// modulus (%) the remainder of the division operating

let	remainder = y % x;
console.log("Result of modulo operator" + remainder);

// Assignment Operator
// Basic Assignment Operator (=)
// The Assignment Operator adds the value of the right opperand to a variable

let assigntmentNumber = 8;

assigntmentNumber = assigntmentNumber + 2;  //assignmentNumber = 10
console.log("Result of addition assignment operator: " + assigntmentNumber);

// This is the shorthand method

assigntmentNumber += 2; //assignmentNumber = 12
console.log("Result of addition assignment operator: " + assigntmentNumber);

// (-=, *=, /=) other assignment operator
assigntmentNumber -= 2;
console.log("Result of substraction assignment operator: " + assigntmentNumber);

assigntmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assigntmentNumber);

assigntmentNumber /= 2;
console.log("Result of division assignment operator: " + assigntmentNumber);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result from MDAS operation: " + mdas);

/*
- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
- The operations were done in the following order:
    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    3. 1 + 2 = 3
    4. 3 - 2.4 = 0.6
*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result from PEMDAS operation: " + pemdas);

/*
- By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
- The operations were done in the following order:
    1. 4 / 5 = 0.8
    2. 2 - 3 = -1
    3. -1 * 0.8 = -0.8
    4. 1 + -.08 = .2
*/

// Increment and Decrement
// Operators that add or subtract values by 1 and re-assign the value of the variable
// Incrementation (++)
// Decrementation (--)

let z = 1;

// The value of "z" is added by a value of one before returning the value and storing it in the variable increment.

// pre-incrementation agad agad plus agad ng 1
let increment = ++z;
console.log("Result of pre-incrementation: " + increment);
console.log("Result of pre-incrementation: " + z);

// post-incrementation kuhanin muna original value then minus 1
increment = z++;
console.log("Result of post-incrementation: " + increment);
console.log("Result of post-incrementation: " + z);

let	decrement = --z;
console.log("Result of pre-decrementation: " + decrement);
console.log("Result of pre-decrementation: " + z);

decrement = z--;
console.log("Result of post-decrementation: " + decrement);
console.log("Result of post-decrementation: " + z);

// Type Coercion

let	numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

/* 
- Adding/concatenating a string and a number will result is a string
- This can be proven in the console by looking at the color of the text displayed
- Black text means that the output returned is a string data type
*/

let numC = 16;
let	numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// In programming languages "true" = 1
// In programming languages "false" = 0

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operator

let	juan = "juan";

// Equality Operator (==)
// Returns boolean value

console.log(1 == 1); //true
console.log(1 == 2); //true
console.log(1 == "1"); //true
console.log(0 == false); //true
console.log("juan" == 'juan'); //true
console.log("juan" == juan); //true

// Inequality Operator(!=) 

console.log(1 != 1); //false
console.log(1 != 2); //false
console.log(1 != "1"); //false
console.log(0 != false); //false
console.log("juan" != 'juan'); //false
console.log("juan" != juan); //false

// Strict Equality Operator (===)
// Checks if operands are equal or have the same content
// Checks operands if are equal or same data type

console.log(1 === "1"); //false
console.log(0 === false); //false

// Strict Inequality Operator (!==)
console.log(1 !== "1"); //true
console.log(0 !== false); //true

// Relational Operators
// Some comparison operators check wether one value is greater than or less than to the other value

let a = 50, b = 65;

// GT or greater than (>)
// LT or less than (<)

let greaterThan = a > b;
console.log(greaterThan); //false

let lessThan = a < b;
console.log(lessThan); //true

let GTorEqual = a >= b;
console.log(GTorEqual); //false

let LTorEqual = a <= b;
console.log(LTorEqual); //true

let numStr = "30";
console.log(a > numStr); //true -> Forced coercion to change string to number
console.log(b <= numStr); //false -> Forced coercion to change string to number

let str = "twenty";
console.log(b >= str); //false -> Since the string is not numeric, the string was converted to a number and it resulted Nan. NaN -> Not a Number

// Logical Operators

let isLegalAge = true;
let isRegistered = false;

// Logical AND "&&" - double Ampersand
// Returns true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR "||" - double pipe
// returns true if one of the operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical NOT "!" - Exclamation Point
// Returns the opposite value

let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);