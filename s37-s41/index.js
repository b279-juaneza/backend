// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose = require("mongoose");
// Allows us to control the App's Cross Origin Resource Sharing
const cors = require("cors");
const userRoutes = require("./Routes/user.js");
const courseRoutes = require("./Routes/course.js");

const app = express();


// MongoDB Connection using SRV Link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.4alz3ct.mongodb.net/Course_Booking_System?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Optional - Validation of DB Connection
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));



// Defines the "/users" string to be included for the user routes defined in the "user.js" route file.
app.use("/users", userRoutes);


// Defines the "/courses" string to be included for the course routes defined in the "course.js" route file.

app.use("/courses", courseRoutes);



// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`));
}

module.exports = app;