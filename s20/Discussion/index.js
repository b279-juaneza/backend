// console.log("Hello World");

// [SECTION] - Looping statements
/*

1. While loop
2. Do-While loop
3. for loop

*/


// while loop
// A while loop takes in an expression/condition
// Expressions are any unit of codes that can be evaluated to a value
// if the condition is true, the statement or the code block will be executed.
// "Iteration" is the term given to the repetition of statements

/*
SYNTAX:

while(expression/condition){
	statement/code blocks
}

*/


let count = 5;
// count = 4; -> 1st iteration
// count = 3; -> 2nd iteration
// count = 2; -> 3rd iteration
// count = 1; -> 4th iteration
// count = 0; -> 5th iteration -> nag false na yung condtion ng while kaya nag stop na mag count

// while the value of count is not equal to 0
while(count !== 0){
	console.log("while: " + count);
	// While: 5 -> 1st iteration
	// While: 4 -> 2nd iteration
	// While: 3 -> 3rd iteration
	// While: 2 -> 4th iteration
	// While: 1 -> 5th iteration
	count --; // decrementation check previous discussion
}

// Do While Loop
/*
SYNTAX:

do{
	statement
} while (expression/condition)

*/

let number = Number(prompt("Give me a number"));

do{
	// the current value is printed out
	console.log("Do while: " + number);
	number++;
} while(number <= 30);

// for loop

/*
SYNTAX:

for(intialization; expression/condition; finalExpression){
	statement/code block
}

*/

for(let count = 0; count <= 20; count++){
	console.log(count);
}

// Accessing of elements of a string
// Individual characters of a string may be accessed using index number
// index number starts from 0

let myString = "alex";
console.log(myString.length); //kinukuha ng .lenght kung ilan yung characters kaya apat lumabas

// Accessing element/character using index number // space is considered as character
console.log(myString[2]); // kaya lumabas and e kasi index number ay 2 (always start count to 0)
console.log(myString[0]);
console.log(myString[3]);

// Will create a loop that will printout the individual letters of string

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}

let myName = "ALEx";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
			console.log(3);
	}else{
		console.log(myName[i]);
	}
}

// [SECTION] Continue and Break Statement

for(let count = 0; count <= 50; count++){
	if(count % 2 === 0){
		continue;
		// This ignores all statements after "continue" keyword
	}

	console.log("continue and break: " + count);

	if(count > 30){
		break;
		// Number values after 10 will no longer be printed in console
		// The process will be terminated
	}
}  

let name = "alexandro";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() == "a"){
		console.log("continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}