// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder

const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		// Sets the name property with the value received from the client postman
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) =>{
		if(error){
			console.log(error);
			return false;
		}else{
			return removedTask;
		}
	})
}


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}

// ACTIVITY

module.exports.getTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){

			console.log(error);
			return false;

		// Find successful, returns the task object back to the client/Postman
		} else {

			return result;	
		}
	})
}
// Solution #1
// Controller function for updating a task status to "complete"

// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Change the status of the document to complete
	3. Save the task
*/

// The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
module.exports.completeTask = (taskId) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, err) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return err;

		}
		// Change the status of the returned document to "complete"
		result.status = "complete";

		// Saves the updated object in the MongoDB database
		// The document already exists in the database and was stored in the "result" parameter, we can use the "save" method to update the existing document with the changes we applied
		// The "return" statement returns the result of the "save" method to the "then" method chained to the "findById" method  which invokes this function
		return result.save().then((updatedTask, saveErr) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr) {

				console.log(saveErr);
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return saveErr;

			// Update successful, returns the updated task object back to the client/Postman
			} else {

				// The "return" statement returns the result to the  "then" method chained to the "save" method which invokes this function
				return updatedTask;

			}
		})
	})

}

// Solution #2
module.exports.completeTask2 = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		if(requestBody.status !== ""){
			if (requestBody.status === 1) {
				result.status = "complete";
			}else if(requestBody.status === 0){
				result.status = "cancelled";
			}else if(requestBody.status === 2){
				result.status = "pending";
			}
		}else{
			return "Please input task status."
		}
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return saveErr;
			} else {
				return updatedTask;
			}
		})
	})
}


