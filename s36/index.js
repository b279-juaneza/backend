// Setup the dependencies/packages
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js")

// Server setup/Middlewares
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add task route
// Allows all the task route created in the taskRoute.js file to use /task
app.use("/tasks", taskRoute);

// DB connection
// connecting to mongoDB atlas
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.4alz3ct.mongodb.net/B279_To-Do?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});








// Server Listen
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;