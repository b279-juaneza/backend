const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type: String,
        required : [true, "PRODUCT NAME is required!"]
    },
    description : {
        type : String,
        required : [true, "PRODUCT DESCRIPTION is required!"]
    },
    price : {
        type : Number,
        required : [true, "PRODUCT PRICE is required!"]
    },
    stocks : {
        type : Number
    },
    image : {
        type : String,
        required : [true, "IMAGE IS REQUIRED!"]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    userOrders : [
            {
                userId : {
                    type : String,
                    required : false
                }
            
            }
        ]
})

module.exports = mongoose.model("product", productSchema);
