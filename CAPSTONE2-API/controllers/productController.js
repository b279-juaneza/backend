const Product = require("../models/productModel");


// Function to create a product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			stocks: data.product.stocks,
			image: data.product.image
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

// Function to retrieve all products (ADMIN ONLY)
module.exports.getAllProducts = (isAdmin) => {
	console.log(isAdmin);
	if(isAdmin) {
		return Product.find({}).then(result => {
		return result;
	})
		}else{
			let message = Promise.resolve("You don't have the access rights to do this action.");

				return message.then((value) => {
				return value
			})
		}
};


// Function to retrieve all Active Products
module.exports.getAllActive = () => {
		return Product.find({isActive : true}).then(result => {
		return result;
	})
};



// Function to retrieve all NOT Active Products
module.exports.getAllNotActive = (isAdmin) => {
	console.log(isAdmin)
	if(isAdmin){
		return Product.find({isActive : false}).then(result => {
		return result;
	})

		}else{
			let message = Promise.resolve("You don't have the access rights to do this action.");

				return message.then((value) => {
				return value
			})
		}
};


// Function to retrieve a single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
};


// Function to update a single product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		stocks : reqBody.stocks,
		image : reqBody.image
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!";
			}
		})
	}
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value

	})
};


// Function to archive a specific product
module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateActiveField = {
		isActive : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});	
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};

module.exports.unArchiveProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updatedActiveField = {
		isActive : true
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updatedActiveField).then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});	
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
};






