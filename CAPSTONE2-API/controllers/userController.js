const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require("../models/productModel")


// Function to register user
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
};


// Function to login user
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};


// Function to request details of a specific user
module.exports.getUser = (userId) => {

    return User.findById(userId).then(result => {
        if (result == null) {
            return false;
        }else {
            result.password = ""

            return result
        }
    })

}


// Function to retrieve all admin users
module.exports.getAllAdmin = () => {
	return User.find({isAdmin : true}).then(result => {
		return result;
	})
}


// Function to check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};



// Funtion to retrieve all users
module.exports.getAllUsers = (isAdmin) => {
	console.log(isAdmin);
	if(isAdmin) {
		return User.find({}).then(result => {
		return result;
	})
		}else{
			let message = Promise.resolve("You don't have the access rights to do this action.");

				return message.then((value) => {
				return value
			})
		}
}



//  Funtion to check out


module.exports.checkout = async (data, reqBody, isAdmin) => {
	console.log(data.isAdmin);
	
	if(data.isAdmin){
			return "You're not allowed to order!"
	}else{
		let isUserUpdated = await User.findById(data.userId).then(user => {
			return Product.findById(data.product.productId).then(result =>{
				console.log(result.name)
			let newOrder = {
				products : [{
					productId : data.product.productId,
					productName : result.name,
					quantity : data.product.quantity
				}],
				totalAmount : result.price * data.product.quantity
			}
			user.orderedProduct.push(newOrder);


		console.log(data.product.productId);

		return user.save().then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
			})

	})

	let isProductUpdated = await Product.findById(data.product.productId).then(product => {
		
		product.userOrders.push({userId: data.userId})

		return product.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})

		})


	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		return false;
	}
	}
}



//SG 1 RETRIEVE ALL ORDERS//

module.exports.getAllOrders = (isAdmin) => {
	if(isAdmin){
		 return User.find({}).then(result =>{
        let orders = []
        result.forEach(user => {
            console.log(user.orderedProduct);
            if (user.orderedProduct[0] !== undefined){
                orders.push({
                    userId : user._id, 
                    orders : user.orderedProduct
                })
            }
        });
        if(result){
            return orders;
        } else {
            return false
        }
    })
		}else{
			return "Admins can only do this request!"
		}
   
};



// SG 2 RETRIEVE USER'S ORDER
module.exports.getOrders = (reqParams) => {
	return User.findById(reqParams.id).then((result) => {
		const products = [];
		let orderedProduct = result.orderedProduct;

		for(let i = 0; i < orderedProduct.length; i++){
			products.push(result.orderedProduct[i].products[0])
		}
		return products;
	})
}