// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");

const app = express();


// MongoDB Connection using SRV Link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.4alz3ct.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));



// Defines the "/users" string to be included for the user routes defined in the "user.js" route file.
app.use("/users", userRoutes);


// Defines the "/courses" string to be included for the course routes defined in the "course.js" route file.

app.use("/products", productRoutes);



// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4001, () => console.log(`API is now online on port ${process.env.PORT || 4001}`));
}

module.exports = app;
