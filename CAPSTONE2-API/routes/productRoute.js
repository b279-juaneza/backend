const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


// Route for creating a new product
router.post("/create", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});




// Route for retrieving all products (ADMIN ONLY)
router.get("/allItems", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});




// Route for retrieving all active products
router.get("/allProducts", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));
});



// Route for retrieving all not active products (ADMIN ONLY)
router.get("/archivedProducts", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.getAllNotActive(isAdmin).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving a  single product
router.get("/:productId", (req, res) => {
    console.log(req.params.productId);
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
}); 


// Route for updating a specific product
router.put("/:productId", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});


// Route for Archiving a specific product
router.put("/:productId/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/unArchive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.unArchiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});












module.exports = router;
