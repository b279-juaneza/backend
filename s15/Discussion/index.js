console.log("Hello World");

// double qoute when using with text

// [Section] Syntax, statements and comments

// to add comment, we are using "//" or CTRL + / -> signle line comment

/*
This
is
a
multi
line
comment

we are using CTRL + SHIFT + /

*/

// SYNTAX AND STATEMENTS (set of rules how to construct)
// JS Statements usually ends with a semi-colon (;)
// Statements -> in programming are instruction that we tell our computer to perform
// semi-colons are not required in JS, but we will use it to help us train to locate where a statements end.

/*[Section] Variables
It is used to contain data.*/

// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
// This makes it easier for us associate information stored in our devices to actual "names" about information

// Variable Declaration 
// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

// Syntax -> var/let/const variableName;
// Variables names should be unique and cannot be repeated

let myVariable;
console.log(myVariable);

// Variable should be declared first before they are used.
// using a variable before they were declared will return an error.
let hello;
console.log(hello);

// CAMEL CASING -> thisIsCamelCasing
// SNAKE CASING -> this_is_snake_casing
// KEBAB CASING -> this-is-kebab-casing

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

// Declaring and Initializing Variable
// SYNTAX ---> let\const variableName = value;

let productName = "desktop computer";
console.log(productName);

// if real numbers no need the double qoute
// integers is what a number is called in js (color purple in console)
let productPrice = 18999;
console.log(productPrice)

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended
// const --> constant

const interest = 3.539;
const hoursInADay = 24;

// Re-assign Value
// Changing it's initial or previous value into another value
// SYNTAX -> variableName = newValue;

productName = "laptop";
console.log(productName);

/* not good
// interest = 5.2;
// console.log(interest)
*/



// let -> the variabale values can be replaced time by time
// const -> the variable values cannot be replaced

// Initializing Variable

// Declare Variable first
let supplier;
// then later you can add the variable value
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "zuitt Store";
console.log(supplier);

/*
Will cause error because the const is missing initializaation(value)

const pi;
pi = 3.1416;
console.log(pi);

*/

// console.log need in the bottom always

let y;
y = 5;
console.log(y);

// HOISTING -> default behaviour of moving declaration on the top. because of -var-

a = 5;
console.log(a);
var a;

// Multiple Variable Declaration
// Usually decalred in one line
// Usually quicker

/*
Invididual Declaration

let	productCode = "DC017";
let	productBrand = "Dell";*/

// multiple declaration 

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

// [SECTION] Data Types

// Strings
// Strings are series of characters that creates a word, a phrase, a sentence or anything related to creating text
// Strings in JS can be written using sing qoute ('') or double qoute ("")
// In other programming languages only the double qoutes used for creating strings.

let country = 'Philippines';
let	province = "Cavite";

// Concatenating strings 
// Multiple strings can be combined to create a single strings using the plus (+) symbol.

// sample output -> Cavite, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

// expected output -> I live in the Philippines
let greeting = "I live in the " + country;
console.log(greeting);

// Escape Characters (\)
// "\n" refers to creating a new line or (enter)

let mailAddress = "Cavite \n\n Philippines";
console.log(mailAddress);

// USing double qoutes along with single qoutes

let	message = "John's employees went home early";
console.log(message);

// "\" used to apostrophe s so that cannot error

message = 'John\'s employees went home early';
console.log(message);

// Numbers 
// Integers/Whole Numbers

let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions

let	grade = 98.7;
console.log(grade);

// Exponential Notation

let	planetDistance = 2e10;
console.log(planetDistance);

// Combining of text and integers

console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain thing.

let	isMarried = false;
let inGoodConduct = true;
console.log("isMarried? " + isMarried);
console.log("inGoodConduct " + inGoodConduct);

// Arrays 
// Arrays are special kind of data type that's used to store multiple values.
// arrays can store different data type but is normally used to store similar data types.
// SYTANX -> let/const arrayName = [valueA, valueB, valueC....];

// same data types

let	grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Different data types
let	details = ["John", "Smith", 32, true];
console.log(details);

// objects
// objects are another special kinds of data type
// It has properties and values -> key pairs

/*
SYNTAX

let\const objectName = {
	propertyA: value, 
	propertyB: value

}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	Contact: ["+6312345", "+6398765"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
};

console.log(person);

// They're also useful for creating abstract objects

let	myGrades = {
	firstgrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 98.2,
	fourthGrading: 94.6
};

console.log(myGrades);

// "typeof" is used to determine the type of data of a variable

console.log(typeof isMarried);
console.log(typeof person);
console.log(typeof grades);

// constant objects and arrays
// if arrays always count start to "0"

const anime = ["one piece", "one punch man", "Attack on titan"];
anime[0] = "kimetsu No Yaiba";
console.log(anime);

// Null
// Null it is used to intentionally express the absence of a value
// This means the variable does not hold any value.
// null, = 0, or = "" are same.

let	spouse = null;
let myNumber = 0;
let	myString = "";

// undefined
// Undefined is represents the state of a variable that has been declared without initial value

console.log(spouse);