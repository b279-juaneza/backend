// console.log("Hello World");


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => {
	let todoTitle = json.map(dataToMap => {
		return {
			json: dataToMap.title
		};
	});
	console.log(todoTitle);
});


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(`The item ${result.title} on the list has a status of ${result.completed}`));



fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({ 
		completed: false, 
		id: 201,
		title: "Created To Do list Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({ 
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do list item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({ 
		Completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});