// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECTION] Inserting documents (Create)

// Insert One document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "09123456789",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});

// Insert Many
/*
Syntax
- db.collectionName.insertMany([{objectA}, {objectB}]);

*/

db.users.insertMany([
    {
    firstName: "Stephen",
    lastName: "hawking",
    age: 76,
    contact: {
        phone: "09123456789",
        email: "stephenhawking@mail.com"
    },
    courses: ["Python", "React", "PHP"],
    department: "none"
    },
     {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "09123456789",
        email: "neilarmstrong@mail.com"
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none"
    }
    ]);

// Finding a single document
// leaving the search criteria emply will retrieve all documents

// Select * FROM users; => SQL

db.users.find({}); // => MongoDB

// SELECT * FROM users WHERE firstname = "Stephen"; => SQL

db.users.find({  firstName: "Stephen"  }); // => MongoDB

// The "pretty" Method allows us to be able to view documents returned by our terminal(robo3T) in a better format

db.users.find({  firstName: "Stephen"  }).pretty();

// Finding documents with multiple parameters

db.users.find({  lastName: "Armstrong"  }, { age: 82 });

// [SECTION] Updating documents (update)

// Updating single documents
// Creating dummy users to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000000",
        email: "Test@mail.com"
    },
    courses: [],
    department: "none"
});

/*

SYNTAX

    db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
    { firstName: "Test" }, 
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "09123456789",
                email: "bill@mai.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations", 
            status: "active"
        }
    }

);

// Updating many documents

db.users.updateMany(
    { department: "none" },
    {
        $set: {
            department: "HR"
        }
    }
);

// Replace one

/*
Syntax
    
    db.collectionName.replaceOne({criteria}, {field: value});

*/


db.users.replaceOne(
    { firstName: "Bill" },
    {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "09123456789",
                email: "bill@mai.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations", 
    }
);


// [SECTION] Deleting documents (Delete)

db.users.insertOne({
    firstName: "test",
});

// Delete Many
db.users.deleteMany({
    firstName: "test"
});


// Delete Single
db.users.deleteOne({
    firstName: "test"
});

db.users.insertOne({
    lastName: "asawa"
});


// [SECTION] Advanced Queries

db.users.find({
    contact: {
        phone: "09123456789",
        email: "stephenhawking@mail.com"
    }
});

// Query on nested field dot notation

db.users.find({"contact.email": "stephenhawking@mail.com"});


// Querying an Array without regard to order

db.users.find({courses: {$all: ["React", "Python"]}});
db.users.find({courses: {$all: ["React"]}});

// Querying an embedded Array

db.users.insertOne({
    nameArray: [{nameA: "Juan"}, {nameB: "Tamad"}]
});