// Use "require" directive to load Node.js modules
// A " Module" let's Node.js transfer data using Hyper Text transfer Protocol (HTTP)
// Clients (browser) and serves (node JS/express JS Applications) communicate by exchanging individual messages.


let http = require("http");

// Using this module's createrServer() method, we can create an HTTP server that listens to requests on specified port.

// A port is a virtual point where network connection start and end.
// Each port is associated with a specific process or service
// ther server will be assigned to port 4000
http.createServer(function (request, response){
	// We use writeHead() method to:
	// Set a status code for message -> 200 means ok -> table nasa bookmark mga status code
	// Set the Content_type of the response as a plain text
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(4000);

console.log("Server running at localhost:4000");