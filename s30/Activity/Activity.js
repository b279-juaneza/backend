

// count the total number of fruits on sale

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "fruitsAvailable", fruitsOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// count the total number of fruits with stock more than or equal to 20

db.fruits.aggregate([
  {$match: {stock: {$gte: 20}}},
  {$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);


// average price of fruits onSale per supplier

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);

// get the highest price of a fruit per supplier

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
  {$sort: {max_price: 1}}
]);

// get the lowest price of a fruit per supplier

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
  {$sort: {min_price: 1}}
]);